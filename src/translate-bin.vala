public class Issue2107.TranslateBin : Gtk.Bin {
	public override void size_allocate (int width, int height, int baseline) {
		var child = get_child ();
		if (child != null && child.visible) {
//			var transform = ((Gsk.Transform) null).translate ({ 0, 1 }); // no crash
//			var transform = ((Gsk.Transform) null).translate ({ 0, 0 }); // no crash
//			var transform = (new Gsk.Transform ()).translate ({ 0, 1 }); // no crash
			var transform = (new Gsk.Transform ()).translate ({ 0, 0 }); // crash
			child.allocate (width, height, baseline, transform);
		}
	}
}
